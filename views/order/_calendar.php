<?php
if ($order->getDateOfRepair()) {
    $selectedYear = $order->getDateOfRepair()->format('Y');
    $selectedMonth = $order->getDateOfRepair()->format('m');
    $selectedDay = number_format($order->getDateOfRepair()->format('d'));
    $date = $order->getDateOfRepair()->format(DateTime::ISO8601);
} else {
    $date = new DateTime();
    $selectedYear = $date->format('Y');
    $selectedMonth = $date->format('m');
    $selectedDay = $date->format('d');
    $date = $date->format(DateTime::ISO8601);
}
?>

<select name="date-year-selector" id="date-year-selector">
    <option disabled="disabled">Выберите год</option>
    <?php foreach (range(1970, date("Y")) as $year): ?>
        <option <?= $selectedYear == $year ? 'selected="true"' : ''; ?> value="<?= $year ?>"><?= $year ?></option>
    <?php endforeach; ?>
</select>

<select name="date-month-selector" id="date-month-selector">
    <option disabled="disabled">Выберите месяц</option>
    <?php
    for ($m = 1; $m <= 12; $m++) :
        $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
        ?>
        <option <?= $selectedMonth == $m ? 'selected="true"' : ''; ?> value="<?= $m ?>"><?= $month ?></option>
    <?php endfor; ?>
</select>

<select name="date-day-selector" id="date-day-selector">
    <option disabled="disabled">Выберите день</option>
    <?php for ($d = 1; $d <= 31; $d++) : ?>
        <option <?= $selectedDay == $d ? 'selected="true"' : ''; ?> value="<?= $d ?>"><?= $d ?></option>
    <?php endfor; ?>
</select>
