<a href="/orders">К списку заказов</a>
<h3>Создание заказа на ремонт летающей тарелки</h3>

<form role="form" name="order-form" action="/order/create" method="POST" enctype="multipart/form-data">

    <?php if ($message): ?>
        <div class="success-message">
            Заказ был успешно сохранен в файл <?= $message ?>
        </div>
    <?php endif ?>

    <div class="form-group">
        <label>Фамилия</label>
        <input name="last-name" type="text" value="<?= $order->getCustomerLastName() ?>">

        <div class="form-error <?= $order->getError('customerLastName') ? 'has-error' : ''; ?>">
            <?= $order->getError('customerLastName') ?>
        </div>
    </div>

    <div class="form-group">
        <label>Имя</label>
        <input name="first-name" type="text" value="<?= $order->getCustomerFirstName() ?>">

        <div class="form-error <?= $order->getError('customerFirstName') ? 'has-error' : ''; ?>">
            <?= $order->getError('customerFirstName') ?>
        </div>
    </div>

    <div class="form-group">
        <label>Номер летающей тарелки</label>
        <input name="vehicle-id" type="text" value="<?= $order->getVehicleId() ?>">

        <div class="form-error <?= $order->getError('vehicleId') ? 'has-error' : ''; ?>">
            <?= $order->getError('vehicleId') ?>
        </div>
    </div>

    <div class="form-group">
        <label>Дата проведения ремонта</label>

        <div class="calendar-container">

            <?php
            /** Include calendar partial */
            include_once('_calendar.php');
            ?>
        </div>

        <input name="repair-date" type="hidden" value="<?= $date ?>">

        <div class="form-error <?= $order->getError('dateOfRepair') ? 'has-error' : ''; ?>">
            <?= $order->getError('dateOfRepair') ?>
        </div>
    </div>

    <div class="form-group">
        <label>Межгалактический ТК номер заказчика</label>
        <input name="customer-id" type="text" value="<?= $order->getCustomerId() ?>">

        <div class="form-error <?= $order->getError('customerId') ? 'has-error' : ''; ?>">
            <?= $order->getError('customerId') ?>
        </div>
    </div>

    <div class="form-group">
        <label>Комментарий</label>
        <input name="comment" type="text" value="<?= $order->getComment() ?>">
    </div>

    <div class="form-group">
        <input type="hidden" name="csrf_token" value="<?= $token ?>">

        <div class="form-error <?= $order->getError('csrf_token') ? 'has-error' : ''; ?>">
            <?= $order->getError('csrf_token') ?>
        </div>
    </div>

    <button type="submit" class="submit-button">Создать</button>
</form>