<?php

use Models\Order;

Class OrderController
{

    public function index()
    {
        return $this->render('views/order/index.php');
    }

    public function create()
    {
        $order = new Order();
        session_start();

        if ($_SESSION['message']) {
            $message = $_SESSION['message'];
            $_SESSION['message'] = '';
        }
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($this->isTokenValid()) {
                $order->bindData($_POST);
                if ($order->isValid()) {
                    $this->exportToXML($order);
                    header("Location: /order/create");
                }
            } else {
                $order->addError('csrf_token', 'Неверный токен. Перезагрузите страницу и повторите ввод.');
            }
        }
        $token = $this->getToken();

        return $this->render('views/order/create.php', [
            'order' => $order,
            'token' => $token,
            'message' => !(empty($message)) ? $message : ''
        ]);
    }

    private function exportToXML($order)
    {
        ob_clean();
        $array = get_object_vars($order);

        $xml = new DOMDocument('1.0');
        $xml->formatOutput = true;

        $order = $xml->createElement('order');
        $order = $xml->appendChild($order);
        foreach ($array as $key => $value) {
            if ($value instanceof DateTime) {
                $value = $value->format('r');
            }
            $newNode = $xml->createElement($key, $value);
            $order->appendChild($newNode);
        }

        $name = mktime();
        $xml->save($name . '.xml');
        header('Content-type: text/xml');
        header('Content-Disposition: attachment; filename=' . basename($name) . '.xml');
        $_SESSION['message'] = $name . '.xml';
        return;
    }

    public function getToken()
    {
        if (!isset($_SESSION['token'])) {
            $token = md5(uniqid(rand(), true));
            $_SESSION['token'] = $token;
            $_SESSION['token_time'] = time();
        } else {
            $token = $_SESSION['token'];
        }
        return $token;
    }

    public function isTokenValid()
    {
        return $_SESSION['token'] === $_POST['csrf_token'];
    }

    public function render($fileName, $variables = array())
    {
        extract($variables);
        ob_start();
        include $fileName;
        $renderedView = ob_get_contents();
        ob_end_clean();
        return $renderedView;
    }
}
