<html>
<head>
    <title>Martian Spaceship Repair Service</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/assets/css/style.css" rel="stylesheet" type="text/css"/>
    <script src="/assets/js/calendar.js"></script>
</head>
<body>
<div class="container">
    <?php
    require_once('routes.php');
    ?>
</div>
</body>
</html>
