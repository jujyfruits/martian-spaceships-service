<?php

namespace Models;

class Order
{

    public $customerLastName;
    public $customerFirstName;
    public $vehicleId;
    public $dateOfRepair;
    public $customerId;
    public $comment;
    private $errors;

    public function getCustomerLastName()
    {
        return $this->customerLastName;
    }

    public function setCustomerLastName($customerLastName)
    {
        $this->customerLastName = $customerLastName;
    }

    public function getCustomerFirstName()
    {
        return $this->customerFirstName;
    }

    public function setCustomerFirstName($customerFirstName)
    {
        $this->customerFirstName = $customerFirstName;
    }

    public function getVehicleId()
    {
        return $this->vehicleId;
    }

    public function setVehicleId($vehicleId)
    {
        $this->vehicleId = $vehicleId;
    }

    public function getDateOfRepair()
    {
        return $this->dateOfRepair;
    }

    public function setDateOfRepair($dateOfRepair)
    {
        $this->dateOfRepair = new \DateTime($dateOfRepair);
    }

    public function getCustomerId()
    {
        return $this->customerId;
    }

    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    private function validateCustomerLastName()
    {
        if ($error = $this->isEmptyField($this->customerLastName)) {
            $this->errors['customerLastName'] = $error;
            return;
        }
        if (!preg_match('/^[a-z -]+$/i', $this->customerLastName)) {
            $this->errors['customerLastName'] = 'Фамилия может содержать только латинские буквы и состоять из нескольких частей, разделенных пробелами или дефисами и начинаться с заглавной буквы.';
        } else {
            $this->customerLastName = ucfirst(strtolower($this->customerLastName));
            $this->customerLastName = preg_replace('/\s{2,}/', ' ', $this->customerLastName);
        }
    }

    private function validateCustomerFirstName()
    {
        if ($error = $this->isEmptyField($this->customerFirstName)) {
            $this->errors['customerFirstName'] = $error;
            return;
        }
        if (!preg_match('/^[a-z -]+$/i', $this->customerFirstName)) {
            $this->errors['customerFirstName'] = 'Имя может содержать только латинские буквы и состоять из нескольких частей, разделенных пробелами или дефисами и начинаться с заглавной буквы.';
        } else {
            $this->customerFirstName = ucfirst(strtolower($this->customerFirstName));
            $this->customerFirstName = preg_replace('/\s{2,}/', ' ', $this->customerFirstName);
        }
    }

    private function validateVehicleId()
    {
        if ($error = $this->isEmptyField($this->vehicleId)) {
            $this->errors['vehicleId'] = $error;
            return;
        }
        if (!preg_match('/^[a-z]{1}\s?[0-9]{5}\s?[a-z]{2}$/i', $this->vehicleId)) {
            $this->errors['vehicleId'] = 'Номер летающей тарелки должен иметь формат x12345xx, где x - это латинские буквы, нехависимо от регистра.';
        } else {
            $this->vehicleId = strtolower($this->vehicleId);
        }
    }

    private function validateDateOfRepair()
    {
        if ($error = $this->isEmptyField($this->dateOfRepair)) {
            $this->errors['dateOfRepair'] = $error;
            return;
        }

        if (!$this->dateOfRepair instanceof \DateTime) {
            $this->errors['dateOfRepair'] = 'Введенная дата невалидна.';
        }
    }

    private function validateCustomerId()
    {
        if ($error = $this->isEmptyField($this->customerId)) {
            $this->errors['customerId'] = $error;
            return;
        }

        if (!preg_match('/^[0-9a-f]{12}$/i', $this->customerId)) {
            $this->errors['customerId'] = 'Межгалактический телекоммуникационный номер заказчика должен состоять из 12 шестнадцатеричных чисел.';
        } else {
            $this->customerId = strtolower($this->customerId);
        }
    }

    private function isEmptyField($field)
    {
        return (!$field) ? 'Поле обязательно к заполнению.' : false;
    }

    public function isValid()
    {
        $this->validateCustomerLastName();
        $this->validateCustomerFirstName();
        $this->validateVehicleId();
        $this->validateDateOfRepair();
        $this->validateCustomerId();
        return sizeof($this->errors) === 0;
    }

    public function getError($param)
    {
        return (!empty($this->errors[$param]) ? $this->errors[$param] : null);
    }

    public function addError($param, $mesage)
    {
        $this->errors[$param] = $mesage;
    }

    public function bindData($data)
    {
        $this->setCustomerLastName($data['last-name']);
        $this->setCustomerFirstName($data['first-name']);
        $this->setVehicleId($data['vehicle-id']);
        $this->setDateOfRepair($data['repair-date']);
        $this->setCustomerId($data['customer-id']);
        $this->setComment($data['comment']);
    }

}
