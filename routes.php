<?php

$controllers = [
    'order' => [
        'index' => [
            '/',
            '/orders'
        ],
        'create' => '/order/create'
    ]
];

function call($className, $action)
{

    $controller = ucfirst($className) . 'Controller';
    require_once('controllers/' . $controller . '.php');
    require_once('models/' . ucfirst($className) . '.php');

    if (class_exists($controller)) {
        $controller = new $controller();
        if (is_callable([$controller, $action])) {
            $result = $controller->{$action}();
            return print($result);
        }
    }
    die("Controller $className doesn't exist");
}

foreach ($controllers as $controller => $actions) {
    foreach ($actions as $action => $urls) {
        $urls = (is_array($urls)) ? $urls : [$urls];
        foreach ($urls as $url) {
            if ($_SERVER['REQUEST_URI'] == $url) {
                return call($controller, $action);
            }
        }
    }
}
return print ('Route was not found');
