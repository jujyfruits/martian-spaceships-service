window.onload = function () {
    var form = document.forms['order-form'];
    var yearSelector = form.elements['date-year-selector'];
    var monthSelector = form.elements['date-month-selector'];
    var daySelector = form.elements['date-day-selector'];
    var repairDate = form.elements['repair-date'];

    yearSelector.onchange = calculateNumberOfDays;
    monthSelector.onchange = calculateNumberOfDays;
    daySelector.onchange = refreshInputValue;

    function calculateNumberOfDays() {
        var numberOfDays = new Date(yearSelector.value, monthSelector.value, 0).getDate();
        if (isNaN(numberOfDays)) {
            return;
        }
        for (var i = 27; i < daySelector.options.length; i++) {
            if (i > numberOfDays) {
                daySelector.options[i].disabled = true;
            } else {
                daySelector.options[i].disabled = false;
            }
        }
        refreshInputValue();
    }

    function refreshInputValue() {
        var newDate = new Date(yearSelector.value, monthSelector.value, daySelector.value);
        if (newDate instanceof Date && isFinite(newDate)) {
            repairDate.value = newDate;
        }
    }
};